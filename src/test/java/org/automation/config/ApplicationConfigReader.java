package org.automation.config;

import org.automation.interfaces.IConfig;
import org.automation.settings.ApplicationConfigKeys;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ApplicationConfigReader implements IConfig {
    private final Properties properties = new Properties();
    private final String userDir;
    public ApplicationConfigReader() throws IOException {
        String filePath = "/src/test/java/resources/global.properties";
        userDir = System.getProperty("user.dir");
        FileInputStream inputStream = new FileInputStream(userDir+filePath);
        properties.load(inputStream);
    }
    @Override
    public BrowserTypes getBrowser() {
        String browser = properties.getProperty(ApplicationConfigKeys.browser);
        return BrowserTypes.valueOf(browser);
    }

    @Override
    public String getURL() {
        return properties.getProperty(ApplicationConfigKeys.url);
    }

    @Override
    public String getChromeDriver() {
        String driverpath = userDir + properties.getProperty(ApplicationConfigKeys.chromeDriver);
        return driverpath;
    }
}
