package org.automation.config;

public enum BrowserTypes {
    Firefox,
    Chrome,
    Safari,
    InternetExplorer,
    Opera
}
