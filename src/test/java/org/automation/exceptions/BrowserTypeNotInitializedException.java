package org.automation.exceptions;

public class BrowserTypeNotInitializedException extends Exception {
    public BrowserTypeNotInitializedException(String error){
        super(error);
    }
}
