Feature: As a user should be able to insert the data into the form and submit

  Background:
    Given I am on the application form page

  @regression
  Scenario Outline: User should be able to submit name and email through the form
    When I enter "<name>" into name field
    And I enter "<email>" into the email field
    And I wait for 10 seconds
    Then I should see the "<name>" in the label

    Examples:
      |name|email|gender|
      |Eranda Kodagoda | eranda.k@eyepax.com|Male|

      @regression
      Scenario Outline: User should be able to submit name and email through the form with gender
        When I enter "<name>" into name field
        And I enter "<email>" into the email field
        And I wait for 10 seconds
        And I select gender "<gender>"
        And I wait for 10 seconds
        Then I should see the "<name>" in the label

        Examples:
          |name|email|gender|
          |Eranda Kodagoda | eranda.k@test.com|Male|