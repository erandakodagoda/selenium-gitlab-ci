package org.automation.helper;

import org.automation.settings.ObjectRepository;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;

public class GenericHelper {
    public static void waitUntilElementToBeVisible(WebElement element){
        Wait<WebDriver> wait = new FluentWait<WebDriver>(ObjectRepository.driver)
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofSeconds(2))
                .ignoring(NoSuchElementException.class);
        WebElement elm = wait.until(driver -> element);
    }
}
