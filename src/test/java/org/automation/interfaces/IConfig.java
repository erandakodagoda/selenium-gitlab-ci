package org.automation.interfaces;

import org.automation.config.BrowserTypes;

public interface IConfig {
    BrowserTypes getBrowser();
    String getURL();
    String getChromeDriver();
}
