package org.automation.pageObjects;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Form2PageObject {
    private WebDriver driver;
    public Form2PageObject(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(id = "exampleCheck1")
    private WebElement select;

    public Form2PageObject getGender(String gender){
        this.select.click();
        System.out.println(gender);
        return this;
    }
}
