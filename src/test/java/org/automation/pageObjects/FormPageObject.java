package org.automation.pageObjects;

import org.junit.Assert;
import org.automation.helper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FormPageObject {
    private WebDriver driver;
    public FormPageObject(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(css = "form > div:nth-child(1) > input")
    private WebElement nameTxtField;
    @FindBy(name = "email")
    private WebElement emailTxtField;
    @FindBy(css = "h4 > input")
    private WebElement inputTxtLabel;

    public FormPageObject setNameTxt(String name){
        this.nameTxtField.sendKeys(name);
        return this;
    }
    public FormPageObject setEmail(String email){
        this.emailTxtField.sendKeys(email);
        return this;
    }
    public void getNameLabelTxt(String name){
        GenericHelper.waitUntilElementToBeVisible(inputTxtLabel);
        String text = inputTxtLabel.getAttribute("value");
        Assert.assertEquals(name,text);
    }
}
