package org.automation.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/org/automation/features",
                    glue = {"org.automation.stepDefinitions","org.automation.support"},
                    monochrome = true,
                    plugin = {"pretty","html:target/cucumber.html","json:target/cucumber.json","junit:target/cukes.xml"},
                    tags = "@regression",
                    publish = true)
public class TestRunner {

}
