package org.automation.settings;

import lombok.Getter;
import lombok.Setter;
import org.automation.interfaces.IConfig;
import org.openqa.selenium.WebDriver;

@Getter
@Setter
public class ObjectRepository {
    public static IConfig iConfig;
    public static WebDriver driver;
}
