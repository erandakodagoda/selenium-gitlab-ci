package org.automation.stepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.automation.config.ApplicationConfigReader;
import org.automation.exceptions.BrowserTypeNotInitializedException;
import org.automation.pageObjects.FormPageObject;
import org.automation.settings.ObjectRepository;
import org.automation.support.BaseClass;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class FillTheFormSteps extends BaseClass {
    private final WebDriver driver = ObjectRepository.driver;
    private final FormPageObject formPageObject = new FormPageObject(driver);
    public FillTheFormSteps() throws IOException, BrowserTypeNotInitializedException {
    }

    @Given("I am on the application form page")
    public void i_am_on_the_application_form_page() {
        try {
            ApplicationConfigReader reader = new ApplicationConfigReader();
            driver.get(reader.getURL());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @When("I enter {string} into name field")
    public void i_enter_into_name_field(String name) {
        formPageObject.setNameTxt(name);
    }
    @When("I enter {string} into the email field")
    public void i_enter_into_the_email_field(String email) {
        formPageObject.setEmail(email);
    }
    @When("I wait for {int} seconds")
    public void i_wait_for_seconds(Integer seconds) throws InterruptedException {
        Thread.sleep(seconds*1000);
    }
    @Then("I should see the {string} in the label")
    public void i_should_see_the_in_the_label(String name) {
        formPageObject.getNameLabelTxt(name);
    }
}
