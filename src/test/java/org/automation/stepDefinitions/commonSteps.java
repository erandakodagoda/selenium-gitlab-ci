package org.automation.stepDefinitions;

import io.cucumber.java.en.When;
import org.automation.exceptions.BrowserTypeNotInitializedException;
import org.automation.pageObjects.Form2PageObject;
import org.automation.settings.ObjectRepository;
import org.automation.support.BaseClass;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class commonSteps extends BaseClass {
    private final WebDriver driver = ObjectRepository.driver;
    private final Form2PageObject form2PageObject = new Form2PageObject(driver);
    public commonSteps() throws IOException, BrowserTypeNotInitializedException {
    }

    @When("I select gender {string}")
    public void i_select_gender(String gender){
        form2PageObject.getGender(gender);
    }
}
