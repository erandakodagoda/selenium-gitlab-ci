package org.automation.support;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.automation.config.ApplicationConfigReader;
import org.automation.exceptions.BrowserTypeNotInitializedException;
import org.automation.settings.ObjectRepository;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


public class BaseClass {
    public static final String USERNAME = "erandakodagoda1";
    public static final String AUTOMATE_KEY = "fwStwfo3RECfBp6dAYiQ";
    public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

    public static ChromeOptions getChromeOptions(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized"); // open Browser in maximized mode
        options.addArguments("disable-infobars"); // disabling infobars
        options.addArguments("--disable-extensions"); // disabling extensions
        options.addArguments("--disable-gpu"); // applicable to windows os only
        options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
        options.addArguments("--no-sandbox"); // Bypass OS security model
        options.addArguments("--headless");
        return options;

    }

    private static WebDriver getChrome() throws MalformedURLException {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(getChromeOptions());
        return driver;
    }

    public void initWebDriver() throws IOException, BrowserTypeNotInitializedException {
        ObjectRepository.iConfig = new ApplicationConfigReader();
        switch (ObjectRepository.iConfig.getBrowser()){
            case Chrome:
                ObjectRepository.driver = getChrome();
                return;
            case Opera:
                throw new BrowserTypeNotInitializedException("Opera Browser is not defined :");
            case Safari:
                throw new BrowserTypeNotInitializedException("Safari Browser is not defined :");
            case Firefox:
                throw new BrowserTypeNotInitializedException("Firefox Browser is not defined :");
            default:
                throw new BrowserTypeNotInitializedException("Invalid Browser Type Specified :"
                        +ObjectRepository.iConfig.getBrowser().toString());
        }
    }
    public static void tearDown(){
        ObjectRepository.driver.close();
        ObjectRepository.driver.quit();
    }
}
