package org.automation.support;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.automation.exceptions.BrowserTypeNotInitializedException;

import java.io.IOException;

public class Hooks extends BaseClass {
    @After
    public void afterExecute(){
        tearDown();
    }
    @Before
    public void beforeExecute() throws IOException, BrowserTypeNotInitializedException {
        initWebDriver();
    }
}
